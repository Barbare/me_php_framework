-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 11, 2022 at 09:37 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `qvizi1`
--

-- --------------------------------------------------------

--
-- Table structure for table `kitxvebi`
--

CREATE TABLE `kitxvebi` (
  `ID` int(5) NOT NULL,
  `kitxva` varchar(200) NOT NULL,
  `pasuxi_1` varchar(50) NOT NULL,
  `pasuxi_2` varchar(50) NOT NULL,
  `pasuxi_3` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kitxvebi`
--

INSERT INTO `kitxvebi` (`ID`, `kitxva`, `pasuxi_1`, `pasuxi_2`, `pasuxi_3`) VALUES
(1, 'რომელია ყველაზე გავრცელებული ენა ბრაზილიაში?', 'გერმანული', 'პორტუგალიური', 'ინგლისური'),
(2, 'რას განასახიერებს თეთრი მტრედი?', 'შიმშილს', 'მშვიდობა', 'ზამთარს'),
(3, 'ვინ არის შრეკის ცოლი?', 'ავრორა', 'ფიონა', 'წითელქუდა'),
(4, ' რა არის ბერძნული ანბანის პირველი ასო?', 'ბეტა', 'ალფა', 'გამა'),
(5, 'ვინ არის მსოფლიოში ყველაზე სწრაფი მიწის ცხოველი?', 'ლომი', 'ჩიტა', 'სპილო'),
(6, 'ვინ არის მსოფლიოში ყველაზე დიდი მიწის ცხოველი?', 'აქლემი', 'სპილო', 'ჟირაფი'),
(7, 'ბალახის ფერი?', 'წითელი', 'მწვანე', 'ლურჯი'),
(8, 'რომელმა ჰოლანდიელმა მხატვარმა მოიჭრა ყურის ნაწილი?', 'პიკასო', 'ვინსენტ ვან გოგი', 'დავინჩი'),
(9, 'რა ენაზე ლაპარაკობენ ბრაზილიის ხალხი?', 'ესპანური', 'პორტუგალიური', 'ფრანგული'),
(10, 'რომელია ძაღლების ყველაზე პატარა ჯიში?', 'ლაბრადორი', 'ჩიხუახუა', 'ნაგაზი');

-- --------------------------------------------------------

--
-- Table structure for table `momxmarebeli`
--

CREATE TABLE `momxmarebeli` (
  `ID` int(5) NOT NULL,
  `saxeli` varchar(30) NOT NULL,
  `gvari` varchar(50) NOT NULL,
  `dab_tarigi` date NOT NULL,
  `piradi_nomeri` int(11) NOT NULL,
  `sqesi` varchar(20) NOT NULL,
  `samushao_adgili` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `momxmarebeli`
--

INSERT INTO `momxmarebeli` (`ID`, `saxeli`, `gvari`, `dab_tarigi`, `piradi_nomeri`, `sqesi`, `samushao_adgili`) VALUES
(1, 'saf', 'asf', '2012-12-12', 12345678, 's', '22'),
(2, 's', 'j', '2012-12-12', 12345678, 's', '23'),
(3, 'a', 'a', '2012-12-12', 12345678, 's', '22');

-- --------------------------------------------------------

--
-- Table structure for table `studenti`
--

CREATE TABLE `studenti` (
  `ID` int(5) NOT NULL,
  `saxeli` varchar(30) NOT NULL,
  `gvari` varchar(50) NOT NULL,
  `saswavlo_kursis_saxeli` varchar(100) NOT NULL,
  `kursis_qula` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `studenti`
--

INSERT INTO `studenti` (`ID`, `saxeli`, `gvari`, `saswavlo_kursis_saxeli`, `kursis_qula`) VALUES
(1, 'elene', 'elenashvili', 'wyalqvesha mexandzre', '100'),
(2, 'ana', 'anaddze', 'sahaero gatxrebis mawarmoebeli', '98'),
(3, 'mari', 'marishvili', 'biznesi', '92'),
(4, 'anka', 'ankadze', 'arqiteqtura', '93'),
(5, 'nika', 'nikadze', 'samedicino', '96');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `kitxvebi`
--
ALTER TABLE `kitxvebi`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `momxmarebeli`
--
ALTER TABLE `momxmarebeli`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `studenti`
--
ALTER TABLE `studenti`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kitxvebi`
--
ALTER TABLE `kitxvebi`
  MODIFY `ID` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `momxmarebeli`
--
ALTER TABLE `momxmarebeli`
  MODIFY `ID` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `studenti`
--
ALTER TABLE `studenti`
  MODIFY `ID` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
